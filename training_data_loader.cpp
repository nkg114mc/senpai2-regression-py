#include <iostream>
#include <memory>
#include <string>
#include <algorithm>
#include <iterator>
#include <future>
#include <mutex>
#include <thread>
#include <deque>
#include <random>

#include "lib/nnue_training_data_formats.h"
#include "lib/nnue_training_data_stream.h"
#include "lib/rng.h"
#include "lib/senp2_eval.h"

#if defined (__x86_64__)
#define EXPORT
#define CDECL
#else
#if defined (_MSC_VER)
#define EXPORT __declspec(dllexport)
#define CDECL __cdecl
#else
#define EXPORT
#define CDECL __attribute__ ((__cdecl__))
#endif
#endif

using namespace binpack;
using namespace chess;

static Square orient(Color color, Square sq)
{
    if (color == Color::White)
    {
        return sq;
    }
    else
    {
        // IMPORTANT: for now we use rotate180 instead of rank flip
        //            for compatibility with the stockfish master branch.
        //            Note that this is inconsistent with nodchip/master.
        return sq.flippedVertically().flippedHorizontally();
    }
}

static Square orient_flip(Color color, Square sq)
{
    if (color == Color::White)
    {
        return sq;
    }
    else
    {
        return sq.flippedVertically();
    }
}

struct HalfKP {
    static constexpr int NUM_SQ = 64;
    static constexpr int NUM_PT = 10;
    static constexpr int NUM_PLANES = (NUM_SQ * NUM_PT + 1);
    static constexpr int INPUTS = NUM_PLANES * NUM_SQ;

    static constexpr int MAX_ACTIVE_FEATURES = 32;

    static int feature_index(Color color, Square ksq, Square sq, Piece p)
    {
        auto p_idx = static_cast<int>(p.type()) * 2 + (p.color() != color);
        return 1 + static_cast<int>(orient(color, sq)) + p_idx * NUM_SQ + static_cast<int>(ksq) * NUM_PLANES;
    }

    static std::pair<int, int> fill_features_sparse(const TrainingDataEntry& e, int* features, float* values, Color color)
    {
        auto& pos = e.pos;
        auto pieces = pos.piecesBB() & ~(pos.piecesBB(Piece(PieceType::King, Color::White)) | pos.piecesBB(Piece(PieceType::King, Color::Black)));
        auto ksq = pos.kingSquare(color);

        // We order the features so that the resulting sparse
        // tensor is coalesced.
        int j = 0;
        for(Square sq : pieces)
        {
            auto p = pos.pieceAt(sq);
            values[j] = 1.0f;
            features[j] = feature_index(color, orient(color, ksq), sq, p);
            ++j;
        }

        return { j, INPUTS };
    }
};

struct SenpaiTwoOriginal {
    static constexpr int HALF_DIM = 759;
    static constexpr int COMPLETE_DIM = HALF_DIM * 2;
    static constexpr int MAX_ACTIVE_FEATURES = COMPLETE_DIM;
};

template <typename T>
struct FeatureSet
{
    static constexpr int INPUTS = T::INPUTS;
    static constexpr int MAX_ACTIVE_FEATURES = T::MAX_ACTIVE_FEATURES;

    static std::pair<int, int> fill_features_sparse(const TrainingDataEntry& e, int* features, float* values, Color color)
    {
        return T::fill_features_sparse(e, features, values, color);
    }
};

struct SparseBatch
{
    static constexpr bool IS_BATCH = true;

    template <typename... Ts>
    SparseBatch(FeatureSet<Ts...>, const std::vector<TrainingDataEntry>& entries)
    {
        num_inputs = FeatureSet<Ts...>::INPUTS;
        size = entries.size();
        is_white = new float[size];
        outcome = new float[size];
        score = new float[size];
        white = new int[size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES];
        black = new int[size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES];
        white_values = new float[size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES];
        black_values = new float[size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES];
        psqt_indices = new int[size];
        layer_stack_indices = new int[size];

        num_active_white_features = 0;
        num_active_black_features = 0;
        max_active_features = FeatureSet<Ts...>::MAX_ACTIVE_FEATURES;

        for (std::size_t i = 0; i < size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES; ++i)
            white[i] = -1;
        for (std::size_t i = 0; i < size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES; ++i)
            black[i] = -1;
        for (std::size_t i = 0; i < size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES; ++i)
            white_values[i] = 0.0f;
        for (std::size_t i = 0; i < size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES; ++i)
            black_values[i] = 0.0f;

        for(int i = 0; i < entries.size(); ++i)
        {
            fill_entry(FeatureSet<Ts...>{}, i, entries[i]);
        }
    }

    int num_inputs;
    int size;

    float* is_white;
    float* outcome;
    float* score;
    int num_active_white_features;
    int num_active_black_features;
    int max_active_features;
    int* white;
    int* black;
    float* white_values;
    float* black_values;
    int* psqt_indices;
    int* layer_stack_indices;

    ~SparseBatch()
    {
        delete[] is_white;
        delete[] outcome;
        delete[] score;
        delete[] white;
        delete[] black;
        delete[] white_values;
        delete[] black_values;
        delete[] psqt_indices;
        delete[] layer_stack_indices;
    }

private:

    template <typename... Ts>
    void fill_entry(FeatureSet<Ts...>, int i, const TrainingDataEntry& e)
    {
        is_white[i] = static_cast<float>(e.pos.sideToMove() == Color::White);
        outcome[i] = (e.result + 1.0f) / 2.0f;
        score[i] = e.score;
        psqt_indices[i] = (e.pos.piecesBB().count() - 1) / 4;
        layer_stack_indices[i] = psqt_indices[i];
        fill_features(FeatureSet<Ts...>{}, i, e);
    }

    template <typename... Ts>
    void fill_features(FeatureSet<Ts...>, int i, const TrainingDataEntry& e)
    {
        const int offset = i * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES;
        num_active_white_features +=
            FeatureSet<Ts...>::fill_features_sparse(e, white + offset, white_values + offset, Color::White)
            .first;
        num_active_black_features +=
            FeatureSet<Ts...>::fill_features_sparse(e, black + offset, black_values + offset, Color::Black)
            .first;
    }
};

struct SenpaiTwoBatch
{
    static constexpr bool IS_BATCH = true;

    template <typename... Ts>
    SenpaiTwoBatch(FeatureSet<Ts...>, const std::vector<TrainingDataEntry>& entries)
    {
        feature_dim = FeatureSet<Ts...>::MAX_ACTIVE_FEATURES;
        size = entries.size();
        is_white = new float[size];
        outcome = new float[size];
        score = new float[size];
        features = new float[size * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES];

        for(int i = 0; i < entries.size(); ++i)
        {
            fill_entry(i, entries[i], i * FeatureSet<Ts...>::MAX_ACTIVE_FEATURES); // fill in feature vector
        }
    }

    int feature_dim;
    int size;

    float* is_white;
    float* outcome;
    float* score;
    float* features;

    ~SenpaiTwoBatch()
    {
        delete[] is_white;
        delete[] outcome;
        delete[] score;
        delete[] features;
    }

private:

    void fill_entry(int i, const TrainingDataEntry& e, int offset)
    {
        is_white[i] = static_cast<float>(e.pos.sideToMove() == Color::White);
        outcome[i] = (e.result + 1.0f) / 2.0f;
        score[i] = e.score;
        fill_features(i, e, offset);
    }

    void fill_features(int i, const TrainingDataEntry& e, int offset)
    {
        senpai::Pos senpai_pos;
        convert_entry_to_pos(senpai_pos, e); // convert

        senpai::EvalVector eval_vec;
        senpai::eval_vectorized(senpai_pos, eval_vec); // featurize

        // compute feature for mg and eg
        eval_vec.toDenseVector(senpai_pos.turn(), features, offset);
    }

public:

    static void convert_entry_to_pos(senpai::Pos &pos, const TrainingDataEntry& tdEntry) {

        const chess::Position & tdBoard = tdEntry.pos;

        senpai::Bit castling_rooks;
        senpai::Bit piece_side[12];

        // init
        std::memset(piece_side, 0, sizeof(piece_side));

        // Side to move
        senpai::Side sideToMove = static_cast<senpai::Side>(tdBoard.sideToMove());

        // Piece placement
        int startIdx = static_cast<int>(whitePawn); // whitePawn = 0
        int endIdx = static_cast<int>(blackKing); // blackKing = 11
        for (int pieceIdx = startIdx; pieceIdx <= endIdx; pieceIdx++) {
            Piece entryPc = Piece::fromId(pieceIdx);
            const Bitboard bb = tdBoard.piecesBB(entryPc);
            for (Square entrySq : bb) {
                senpai::Square senpaiSq = sf_square_to_senp_square(entrySq);
                senpai::bit::set(piece_side[pieceIdx], senpaiSq);
            }
        }

        // Castling availability.
        castling_rooks = (senpai::Bit)(0);
        CastlingRights crights = tdBoard.castlingRights();
        if (contains(crights, CastlingRights::WhiteKingSide)) {
            senpai::bit::set(castling_rooks, senpai::H1);
        }
        if (contains(crights, CastlingRights::WhiteQueenSide)) {
            senpai::bit::set(castling_rooks, senpai::A1);
        }
        if (contains(crights, CastlingRights::BlackKingSide)) {
            senpai::bit::set(castling_rooks, senpai::H8);
        }
        if (contains(crights, CastlingRights::BlackQueenSide)) {
            senpai::bit::set(castling_rooks, senpai::A8);
        }

        // En passant square. Ignore if no pawn capture is possible
        senpai::Square epSqr = senpai::Square_None;
        if (tdBoard.epSquare() != Square::none()) {
            epSqr = sf_square_to_senp_square(tdBoard.epSquare());
        }

        // Halfmove clock
        int sf_rule50 = static_cast<int>(tdBoard.rule50Counter());

        // Fullmove number
        int full_mv_count = static_cast<int>(tdBoard.fullMove());
        int gamePly = static_cast<int>(tdBoard.ply());

        senpai::create_pos(pos, sideToMove, piece_side, castling_rooks, epSqr);

        pos.p_rule50_count = sf_rule50;
        pos.p_fullmv_count = full_mv_count;
    }

    static senpai::Square sf_square_to_senp_square(chess::Square sfSquare) {
        int sf_sq = int(sfSquare);
        senpai::File fl = senpai::File(sf_sq & 0x7);
        senpai::Rank rk = senpai::Rank(sf_sq >> 3);
        return senpai::square_make(fl, rk);
    }

};


template <typename StorageT>
struct Stream
{
    using StorageType = StorageT;

    Stream(int concurrency, const char* filename, bool cyclic, std::function<bool(const TrainingDataEntry&)> skipPredicate) :
        m_stream(training_data::open_sfen_input_file_parallel(concurrency, filename, cyclic, skipPredicate))
    {
    }

    virtual ~Stream() = default;
    virtual StorageT* next() = 0;

protected:
    std::unique_ptr<training_data::BasicSfenInputStream> m_stream;
};

template <typename FeatureSetT, typename StorageT>
struct FeaturedBatchStream : Stream<StorageT>
{
    static_assert(StorageT::IS_BATCH);

    using FeatureSet = FeatureSetT;
    using BaseType = Stream<StorageT>;

    static constexpr int num_feature_threads_per_reading_thread = 2;

    FeaturedBatchStream(int concurrency, const char* filename, int batch_size, bool cyclic, std::function<bool(const TrainingDataEntry&)> skipPredicate) :
        BaseType(
            std::max(
                1,
                concurrency / num_feature_threads_per_reading_thread
            ),
            filename,
            cyclic,
            skipPredicate
        ),
        m_concurrency(concurrency),
        m_batch_size(batch_size)
    {
        m_stop_flag.store(false);

        auto worker = [this]()
        {
            std::vector<TrainingDataEntry> entries;
            entries.reserve(m_batch_size);

            while(!m_stop_flag.load())
            {
                entries.clear();

                {
                    std::unique_lock lock(m_stream_mutex);
                    BaseType::m_stream->fill(entries, m_batch_size);
                    if (entries.empty())
                    {
                        break;
                    }
                }

                auto batch = new StorageT(FeatureSet{}, entries);

                {
                    std::unique_lock lock(m_batch_mutex);
                    m_batches_not_full.wait(lock, [this]() { return m_batches.size() < m_concurrency + 1 || m_stop_flag.load(); });

                    m_batches.emplace_back(batch);

                    lock.unlock();
                    m_batches_any.notify_one();
                }

            }
            m_num_workers.fetch_sub(1);
            m_batches_any.notify_one();
        };

        const int num_feature_threads = std::max(
            1,
            concurrency - std::max(1, concurrency / num_feature_threads_per_reading_thread)
        );

        for (int i = 0; i < num_feature_threads; ++i)
        {
            m_workers.emplace_back(worker);

            // This cannot be done in the thread worker. We need
            // to have a guarantee that this is incremented, but if
            // we did it in the worker there's no guarantee
            // that it executed.
            m_num_workers.fetch_add(1);
        }
    }

    StorageT* next() override
    {
        std::unique_lock lock(m_batch_mutex);
        m_batches_any.wait(lock, [this]() { return !m_batches.empty() || m_num_workers.load() == 0; });

        if (!m_batches.empty())
        {
            auto batch = m_batches.front();
            m_batches.pop_front();

            lock.unlock();
            m_batches_not_full.notify_one();

            return batch;
        }
        return nullptr;
    }

    ~FeaturedBatchStream()
    {
        m_stop_flag.store(true);
        m_batches_not_full.notify_all();

        for (auto& worker : m_workers)
        {
            if (worker.joinable())
            {
                worker.join();
            }
        }

        for (auto& batch : m_batches)
        {
            delete batch;
        }
    }

private:
    int m_batch_size;
    int m_concurrency;
    std::deque<StorageT*> m_batches;
    std::mutex m_batch_mutex;
    std::mutex m_stream_mutex;
    std::condition_variable m_batches_not_full;
    std::condition_variable m_batches_any;
    std::atomic_bool m_stop_flag;
    std::atomic_int m_num_workers;

    std::vector<std::thread> m_workers;
};

std::function<bool(const TrainingDataEntry&)> make_skip_predicate(bool filtered, int random_fen_skipping)
{
    if (filtered || random_fen_skipping)
    {
        return [
            random_fen_skipping,
            prob = double(random_fen_skipping) / (random_fen_skipping + 1),
            filtered
            ](const TrainingDataEntry& e){

            auto do_skip = [&]() {
                std::bernoulli_distribution distrib(prob);
                auto& prng = rng::get_thread_local_rng();
                return distrib(prng);
            };

            auto do_filter = [&]() {
                return (e.isCapturingMove() || e.isInCheck());
            };

            static thread_local std::mt19937 gen(std::random_device{}());
            return (random_fen_skipping && do_skip()) || (filtered && do_filter());
        };
    }

    return nullptr;
}

extern "C" {

    EXPORT Stream<SparseBatch>* CDECL create_sparse_batch_stream(const char* feature_set_c, int concurrency, const char* filename, int batch_size, bool cyclic, bool filtered, int random_fen_skipping)
    {
        auto skipPredicate = make_skip_predicate(filtered, random_fen_skipping);

        std::string_view feature_set(feature_set_c);
        if (feature_set == "HalfKP")
        {
            return new FeaturedBatchStream<FeatureSet<HalfKP>, SparseBatch>(concurrency, filename, batch_size, cyclic, skipPredicate);
        }
        fprintf(stderr, "Unknown feature_set %s\n", feature_set_c);
        return nullptr;
    }

    EXPORT void CDECL destroy_sparse_batch_stream(Stream<SparseBatch>* stream)
    {
        delete stream;
    }

    EXPORT SparseBatch* CDECL fetch_next_sparse_batch(Stream<SparseBatch>* stream)
    {
        return stream->next();
    }

    EXPORT void CDECL destroy_sparse_batch(SparseBatch* e)
    {
        delete e;
    }

    // Senpai inferfaces

    EXPORT Stream<SenpaiTwoBatch>* CDECL create_senpai_batch_stream(const char* feature_set_c, int concurrency, const char* filename, int batch_size, bool cyclic, bool filtered, int random_fen_skipping)
    {
        auto skipPredicate = make_skip_predicate(filtered, random_fen_skipping);

        std::string_view feature_set(feature_set_c);
        if (feature_set == "Senpai2") {
            return new FeaturedBatchStream<FeatureSet<SenpaiTwoOriginal>, SenpaiTwoBatch>(concurrency, filename, batch_size, cyclic, skipPredicate);
        }
        fprintf(stderr, "Unknown feature_set %s\n", feature_set_c);
        return nullptr;
    }

    EXPORT void CDECL destroy_senpai_batch_stream(Stream<SenpaiTwoBatch>* stream)
    {
        delete stream;
    }

    EXPORT SenpaiTwoBatch* CDECL fetch_next_senpai_batch(Stream<SenpaiTwoBatch>* stream)
    {
        return stream->next();
    }

    EXPORT void CDECL destroy_senpai_batch(SenpaiTwoBatch* e)
    {
        delete e;
    }

    EXPORT void CDECL senpai_init_all(SenpaiTwoBatch* e)
    {
        senpai::senpai_eval_init();
    }

}

/* benches */ //*
#include <chrono>

void read_sfen_file(std::string input_filename) {

    senpai::clear_pawn_table();

	std::ifstream fs(input_filename, std::ios::binary);

    int64_t cnt = 0;
    senpai::Pos senpai_pos;

	while (fs)
	{
        binpack::nodchip::PackedSfenValue p;
		if (fs.read((char*)&p, sizeof(binpack::nodchip::PackedSfenValue))) {
            cnt++;
            TrainingDataEntry entry = packedSfenValueToTrainingDataEntry(p);
            SenpaiTwoBatch::convert_entry_to_pos(senpai_pos, entry);
            
            std::string fenstr1 = entry.pos.fen();
            std::string fenstr2 = pos_to_fen(senpai_pos);
            if (fenstr1 != fenstr2) {
                std::cout << cnt << ": " << fenstr1 << std::endl;
                std::cout << cnt << ": " << fenstr2 << std::endl;
            }

            if (cnt % 1000000 == 0) {
                std::cout << "Done checking " << cnt << " positions." << std::endl;
            }
		} else {
            std::cout << "Not enough bits read..." << std::endl;
			break;
		}
	}

}

int main()
{
    /*
    auto stream = create_sparse_batch_stream("HalfKP", 4, "10m_d3_q_2.binpack", 8192, true, false, 0);
    auto t0 = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < 1000; ++i)
    {
        if (i % 100 == 0) std::cout << i << '\n';
        destroy_sparse_batch(stream->next());
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    std::cout << (t1 - t0).count() / 1e9 << "s\n";
    */

    senpai::senpai_eval_init();

    read_sfen_file("/media/mc/Fastdata/Stockfish-NNUE/train2b_d9_vondele/train_d9_2b_20210902_vondele.bin");

    return 0;
}
//*/