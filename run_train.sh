python3 train_linear.py \
    /media/mc/Fastdata/Stockfish-NNUE/trainingdata100m/trn_100m_d10.bin \
    /media/mc/Fastdata/Stockfish-NNUE/validate1m/val_1m_d14.bin \
    --threads 1 \
    --num-workers 32 \
    --batch-size 40000 \
    --random-fen-skipping 3 \
    --lambda=1.0 \
    --max_epochs=400