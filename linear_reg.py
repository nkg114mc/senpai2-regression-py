import ranger
import torch
from torch import nn
import torch.nn.functional as F
import pytorch_lightning as pl
import copy
import numpy as np


def fetch_weight_parameters(layers):
    pass


class MyLinearModel(pl.LightningModule):
    """
    This model attempts to directly represent the nodchip Stockfish trainer methodology.

    lambda_ = 0.0 - purely based on game results
    lambda_ = 1.0 - purely based on search scores

    It is not ideal for training a Pytorch quantized model directly.
    """

    def __init__(self, feature_dim, lambda_=1.0):
        super(MyLinearModel, self).__init__()
        # self.linear1 = nn.Linear(feature_dim, 1, bias=True)
        self.linear1 = nn.Linear(feature_dim, 1, bias=False)
        self.lambda_ = lambda_

    def forward(self, x):
        yhat = self.linear1.forward(x)
        return yhat

    def step_nnue_(self, batch, batch_idx, loss_type):
        # self._clip_weights()

        us, them, outcome, score, feature_values = batch

        # 600 is the kPonanzaConstant scaling factor needed to convert the training net output to a score.
        # This needs to match the value used in the serializer
        nnue2score = 600
        in_scaling = 410
        out_scaling = 361

        q = (self(feature_values) * nnue2score / out_scaling).sigmoid()
        t = outcome
        p = (score / in_scaling).sigmoid()

        pt = p * self.lambda_ + t * (1.0 - self.lambda_)

        loss = torch.pow(torch.abs(pt - q), 2.6).mean()

        self.log(loss_type, loss)

        return loss

        # MSE Loss function for debugging
        # Scale score by 600.0 to match the expected NNUE scaling factor
        # output = self(us, them, white, black) * 600.0
        # loss = F.mse_loss(output, score)

    def step_(self, batch, batch_idx, loss_type):
        us, them, outcome, score, feature_values = batch

        #scaling = 410
        #q = (self(feature_values) / scaling).sigmoid()
        #t = outcome
        #p = (score / scaling).sigmoid()

        #pt = p * self.lambda_ + t * (1.0 - self.lambda_)

        q = self(feature_values)
        p = score
        loss = torch.pow(torch.abs(p - q), 2).mean()

        self.log(loss_type, loss)
        return loss

    def training_step(self, batch, batch_idx):
        return self.step_(batch, batch_idx, 'train_loss')

    def validation_step(self, batch, batch_idx):
        self.step_(batch, batch_idx, 'val_loss')

    def test_step(self, batch, batch_idx):
        self.step_(batch, batch_idx, 'test_loss')

    def configure_optimizers(self):
        # Train with a lower LR on the output layer
        """
        LR = 8.75e-4
        train_params = [
            {'params': get_parameters([self.input]), 'lr': LR, 'gc_dim': 0},
            {'params': [self.layer_stacks.l1_fact.weight], 'lr': LR},
            {'params': [self.layer_stacks.l1.weight], 'lr': LR},
            {'params': [self.layer_stacks.l1.bias], 'lr': LR},
            {'params': [self.layer_stacks.l2.weight], 'lr': LR},
            {'params': [self.layer_stacks.l2.bias], 'lr': LR},
            {'params': [self.layer_stacks.output.weight], 'lr': LR},
            {'params': [self.layer_stacks.output.bias], 'lr': LR},
        ]
        # increasing the eps leads to less saturated nets with a few dead neurons
        optimizer = ranger.Ranger(train_params, betas=(.9, 0.999), eps=1.0e-7, gc_loc=False, use_gc=False)
        # Drop learning rate after 75 epochs
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.992)
        return [optimizer], [scheduler]
        """
        #return torch.optim.SGD(self.parameters(), lr=0.02)
        return torch.optim.Adam(self.parameters(), lr=0.02, weight_decay=0.001)

    def load_txt_weight(self, weight_file):
        w_mg = [0] * 1000
        w_eg = [0] * 1000
        # load from file
        print("Load file from weight file:", weight_file)
        with open(weight_file) as fp:
            lines = fp.readlines()
            count = 0
            for line in lines:
                mg_val, eg_val = _parse_line(line)
                w_mg[count] = float(mg_val) / 100.00
                w_eg[count] = float(eg_val) / 100.00
                # print("Line{}: {}".format(count, line.strip()))
                # print(count, mg_val, eg_val)
                count += 1

        weight_list = []
        for i in range(0, count):
            weight_list.append(w_mg[i])
        for i in range(0, count):
            weight_list.append(w_eg[i])

        # assign weight
        with torch.no_grad():
            print(self.linear1.weight.shape)
            new_weights = torch.Tensor(np.array(weight_list))
            print(new_weights.shape)
            print(new_weights, len(new_weights))
            self.linear1.weight.copy_(new_weights)
            print(self.linear1.weight.shape)

    @staticmethod
    def _parse_line(line_str) -> (int, int):
        result = ""
        for i in range(0, len(line_str)):
            c = line_str[i]
            if '0' <= c <= '9':
                result += c
            elif c == '-':
                result += c
            else:
                result += ' '
        tokens = result.strip().split()
        if len(tokens) != 2:
            raise Exception("Can not parse line: {}".format(result))
        assert len(tokens) == 2
        return int(tokens[0]), int(tokens[1])

    @staticmethod
    def _rounding_float(flt_val) -> int:
        return int(flt_val)

    def write_to_file(self, file_name):
        weight = self.linear1.weight
        bias = self.linear1.bias
        ary = weight.detach().numpy()
        weight_dim = ary.shape[1]
        half_dim = weight_dim // 2

        weight_file = open(file_name, "w")
        for idx in range(0, half_dim):
            mg_wght = self._rounding_float(ary[0][idx] * 100)
            eg_wght = self._rounding_float(ary[0][idx + half_dim] * 100)
            print("\tScore_Pair({},{}),".format(mg_wght, eg_wght))
            weight_file.write("\tScore_Pair({},{}),\n".format(mg_wght, eg_wght))
        # print(ary)
        weight_file.close()

        print("Dump weight vector to file {} with dimension {}".format(file_name, weight_dim))


if __name__ == '__main__':
    main_train()
