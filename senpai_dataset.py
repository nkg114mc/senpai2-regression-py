from abc import ABC

import numpy as np
import ctypes
import torch
import os
import sys
import glob
from torch.utils.data import Dataset
from nnue_dataset import TrainingDataProvider

local_dllpath = [n for n in glob.glob('./*training_data_loader.*') if
                 n.endswith('.so') or n.endswith('.dll') or n.endswith('.dylib')]
if not local_dllpath:
    print('Cannot find data_loader shared library.')
    sys.exit(1)
dllpath = os.path.abspath(local_dllpath[0])
dll = ctypes.cdll.LoadLibrary(dllpath)


class SenpaiTwoBatch(ctypes.Structure):
    _fields_ = [
        ('feature_dim', ctypes.c_int),
        ('size', ctypes.c_int),
        ('is_white', ctypes.POINTER(ctypes.c_float)),
        ('outcome', ctypes.POINTER(ctypes.c_float)),
        ('score', ctypes.POINTER(ctypes.c_float)),
        ('features', ctypes.POINTER(ctypes.c_float)),
    ]

    def get_tensors(self, device):
        feature_values = torch.from_numpy(np.ctypeslib.as_array(self.features, shape=(self.size, self.feature_dim))).pin_memory().to(device=device, non_blocking=True)
        us = torch.from_numpy(np.ctypeslib.as_array(self.is_white, shape=(self.size, 1))).pin_memory().to(device=device, non_blocking=True)
        them = 1.0 - us
        outcome = torch.from_numpy(np.ctypeslib.as_array(self.outcome, shape=(self.size, 1))).pin_memory().to(device=device, non_blocking=True)
        score = torch.from_numpy(np.ctypeslib.as_array(self.score, shape=(self.size, 1))).pin_memory().to(device=device, non_blocking=True)
        return us, them, outcome, score, feature_values


SenpaiTwoBatchPtr = ctypes.POINTER(SenpaiTwoBatch)

create_senpai_batch_stream = dll.create_senpai_batch_stream
create_senpai_batch_stream.restype = ctypes.c_void_p
create_senpai_batch_stream.argtypes = [ctypes.c_char_p, ctypes.c_int, ctypes.c_char_p, ctypes.c_int, ctypes.c_bool,
                                       ctypes.c_bool, ctypes.c_int]

destroy_senpai_batch_stream = dll.destroy_sparse_batch_stream
destroy_senpai_batch_stream.argtypes = [ctypes.c_void_p]

fetch_next_senpai_batch = dll.fetch_next_senpai_batch
fetch_next_senpai_batch.restype = SenpaiTwoBatchPtr
fetch_next_senpai_batch.argtypes = [ctypes.c_void_p]

destroy_senpai_batch = dll.destroy_senpai_batch
senpai_init_all = dll.senpai_init_all


class SenpaiTwoBatchProvider(TrainingDataProvider):
    def __init__(self, feature_set, filename, batch_size, cyclic=True, num_workers=1, filtered=False,
                 random_fen_skipping=0, device='cpu'):
        super(SenpaiTwoBatchProvider, self).__init__(
            feature_set,
            create_senpai_batch_stream,
            destroy_senpai_batch_stream,
            fetch_next_senpai_batch,
            destroy_senpai_batch,
            filename,
            cyclic,
            num_workers,
            batch_size,
            filtered,
            random_fen_skipping,
            device)


class SenpaiTwoBatchDataset(torch.utils.data.IterableDataset):
    def __init__(self, feature_set, filename, batch_size, cyclic=True, num_workers=1, filtered=False,
                 random_fen_skipping=0, device='cpu'):
        super(SenpaiTwoBatchDataset).__init__()
        self.feature_set = feature_set
        self.filename = filename
        self.batch_size = batch_size
        self.cyclic = cyclic
        self.num_workers = num_workers
        self.filtered = filtered
        self.random_fen_skipping = random_fen_skipping
        self.device = device

    def __iter__(self):
        return SenpaiTwoBatchProvider(self.feature_set, self.filename, self.batch_size, cyclic=self.cyclic,
                                      num_workers=self.num_workers, filtered=self.filtered,
                                      random_fen_skipping=self.random_fen_skipping, device=self.device)
