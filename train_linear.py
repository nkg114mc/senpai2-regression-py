import argparse
import pytorch_lightning as pl
import os
import torch
import nnue_dataset
import features
import senpai2_feature
import senpai_dataset
import linear_reg
from torch import set_num_threads as t_set_num_threads
from pytorch_lightning import loggers as pl_loggers
from torch.utils.data import DataLoader, Dataset


def make_senpai_data_loaders(train_filename, val_filename, num_workers, batch_size, filtered, random_fen_skipping,
                             main_device):
    # Initialize featurizer
    senpai_dataset.senpai_init_all()

    # print some parameters
    print("Data loader num_workers = {}".format(num_workers))

    # Epoch and validation sizes are arbitrary
    epoch_size = 100000000
    val_size = 1000000
    features_name = 'Senpai2'  # feature_set.name
    train_stream = senpai_dataset.SenpaiTwoBatchDataset(features_name, train_filename, batch_size,
                                                        num_workers=num_workers,
                                                        filtered=filtered,
                                                        random_fen_skipping=random_fen_skipping,
                                                        device=main_device)
    val_stream = senpai_dataset.SenpaiTwoBatchDataset(features_name, val_filename, batch_size, filtered=filtered,
                                                      random_fen_skipping=random_fen_skipping, device=main_device)
    train = DataLoader(nnue_dataset.FixedNumBatchesDataset(train_stream, (epoch_size + batch_size - 1) // batch_size),
                       batch_size=None, batch_sampler=None)
    val = DataLoader(nnue_dataset.FixedNumBatchesDataset(val_stream, (val_size + batch_size - 1) // batch_size),
                     batch_size=None, batch_sampler=None)
    return train, val


def main_train():
    parser = argparse.ArgumentParser(description="Trains the network.")
    parser.add_argument("train", help="Training data (.bin or .binpack)")
    parser.add_argument("val", help="Validation data (.bin or .binpack)")
    parser = pl.Trainer.add_argparse_args(parser)
    parser.add_argument("--lambda", default=1.0, type=float, dest='lambda_',
                        help="lambda=1.0 = train on evaluations, lambda=0.0 = train on game results, "
                             "interpolates between (default=1.0).")
    parser.add_argument("--num-workers", default=1, type=int, dest='num_workers',
                        help="Number of worker threads to use for data loading. Currently only works well for binpack.")
    parser.add_argument("--batch-size", default=-1, type=int, dest='batch_size',
                        help="Number of positions per batch / per iteration. Default on GPU = 8192 on CPU = 128.")
    parser.add_argument("--threads", default=-1, type=int, dest='threads',
                        help="Number of torch threads to use. Default automatic (cores) .")
    parser.add_argument("--seed", default=42, type=int, dest='seed', help="torch seed to use.")
    parser.add_argument("--smart-fen-skipping", action='store_true', dest='smart_fen_skipping_deprecated',
                        help="If enabled positions that are bad training targets will be skipped during loading. "
                             "Default: True, kept for backwards compatibility. This option is ignored")
    parser.add_argument("--no-smart-fen-skipping", action='store_true', dest='no_smart_fen_skipping',
                        help="If used then no smart fen skipping will be done. By default smart fen skipping is done.")
    parser.add_argument("--random-fen-skipping", default=3, type=int, dest='random_fen_skipping',
                        help="skip fens randomly on average random_fen_skipping before using one.")
    parser.add_argument("--resume-from-model", dest='resume_from_model',
                        help="Initializes training using the weights from the given .pt model")
    features.add_argparse_args(parser)
    args = parser.parse_args()

    if not os.path.exists(args.train):
        raise Exception('{0} does not exist'.format(args.train))
    if not os.path.exists(args.val):
        raise Exception('{0} does not exist'.format(args.val))

    if args.resume_from_model is None:
        linear_weight = linear_reg.MyLinearModel(senpai2_feature.FEATURE_DIM)
    else:
        linear_weight = torch.load(args.resume_from_model)
        linear_weight.lambda_ = args.lambda_

    print("Training with {} validating with {}".format(args.train, args.val))

    pl.seed_everything(args.seed)
    print("Seed {}".format(args.seed))

    batch_size = args.batch_size
    if batch_size <= 0:
        batch_size = 16384
    print('Using batch size {}'.format(batch_size))

    # about filtering
    print('Smart fen skipping: {}'.format(not args.no_smart_fen_skipping))
    print('Random fen skipping: {}'.format(args.random_fen_skipping))

    if args.threads > 0:
        print('limiting torch to {} threads.'.format(args.threads))
        t_set_num_threads(args.threads)

    main_device = 'gpu'
    logdir = args.default_root_dir if args.default_root_dir else 'logs/'
    print('Using log dir {}'.format(logdir), flush=True)

    tb_logger = pl_loggers.TensorBoardLogger(logdir)
    checkpoint_callback = pl.callbacks.ModelCheckpoint(save_last=True, save_top_k=-1)
    trainer = pl.Trainer.from_argparse_args(args, callbacks=[checkpoint_callback], logger=tb_logger)

    #main_device = trainer.stra.root_device if trainer.root_gpu is None else 'cuda:' + str(trainer.root_gpu)
    #main_device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    main_device = torch.device("cpu")
    print(main_device)

    linear_weight.to(device=main_device)

    print('Using c++ data loader')
    train_ds, val_ds = make_senpai_data_loaders(args.train, args.val, args.num_workers, batch_size,
                                                not args.no_smart_fen_skipping, args.random_fen_skipping, main_device)

    trainer.fit(linear_weight, train_ds, val_ds)


if __name__ == '__main__':
    main_train()
