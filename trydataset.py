import nnue_dataset
import senpai2_feature
import senpai_dataset
import pytorch_lightning as pl
import os
import torch
import pandas as pd
import numpy as np
from torch.utils.data import DataLoader, Dataset
from linear_reg import MyLinearModel


def build_test_data_loader(train_filename, num_workers, batch_size, filtered, random_fen_skipping, main_device):
    epoch_size = 100000
    train_infinite = senpai_dataset.SenpaiTwoBatchDataset('Senpai2', train_filename, batch_size,
                                                          num_workers=num_workers,
                                                          filtered=filtered,
                                                          random_fen_skipping=random_fen_skipping,
                                                          device=main_device)
    # num_workers has to be 0 for sparse, and 1 for dense
    # it currently cannot work in parallel mode but it shouldn't need to
    train_loader = DataLoader(
        nnue_dataset.FixedNumBatchesDataset(train_infinite, (epoch_size + batch_size - 1) // batch_size),
        batch_size=None, batch_sampler=None)
    return train_loader


def main_run_data_loader():
    weight_dim = 1518
    train_filename = '/home/mc/sidework/nnchess/senpai2_regression/scores.bin'  # '/media/mc/Fastdata/Stockfish-NNUE/validate1m/val_1m_d14.bin'
    num_workers = 1
    batch_size = 10  # 16384
    filtered = False
    random_fen_skipping = False
    main_device = 'cpu'

    trn_loader = build_test_data_loader(train_filename, num_workers, batch_size, filtered, random_fen_skipping,
                                        main_device)

    # model
    linear_model = MyLinearModel(weight_dim, 1.0)
    linear_model.load_txt_weight("/home/mc/sidework/nnchess/senpai2_regression/weights_init_original.txt")

    print("Loader created...")
    for batch_idx, sample in enumerate(trn_loader):
        # print(batch_idx, sample[4].shape)
        # x_df = pd.DataFrame(sample[4])
        # x_df.to_csv('tmp.csv', index=False, header=False)
        yhat = linear_model.forward(sample[4])
        print(sample[3])
        print(yhat)

        x = torch.transpose(sample[4], 0, 1)
        print(x)
        x_df = pd.DataFrame(x)
        x_df.to_csv('tmp.csv', index=False, header=False)
        break

    print("Finish.")


def main_run_model_weight_test():
    weight_dim = 1518
    linear_model = MyLinearModel(weight_dim, 1.0)
    weight = linear_model.linear1.weight
    ary = weight.detach().numpy()
    bias = linear_model.linear1.bias

    half_dim = weight_dim // 2
    for idx in range(0, half_dim):
        mg_wght = (ary[0][idx] * 100)
        eg_wght = (ary[0][idx + half_dim] * 100)
        print("\tScore_Pair({},{}),".format(mg_wght, eg_wght))
    print(ary)

    # w_df.to_csv('weight.csv', index=False, header=True)
    print(weight)
    print(bias)


def main_model_weight_write_file():
    weight_dim = 1518
    linear_model = MyLinearModel(weight_dim, 1.0)
    linear_model.write_to_file("dummy_weight.txt")


def main_ckpt_to_weight_file():
    model = MyLinearModel.load_from_checkpoint(
        '/home/mc/sidework/nnchess/senpai2-regression-py/logs/lightning_logs/version_0/checkpoints/last.ckpt',
        feature_dim=senpai2_feature.FEATURE_DIM)
    model.eval()
    model.write_to_file("ckpt_weights.txt")


if __name__ == '__main__':
    senpai_dataset.senpai_init_all()
    # main_run_data_loader()
    # main_run_model_weight_test()
    # main_model_weight_write_file()
    main_ckpt_to_weight_file()
